@extends('layout.default')

@section('content')
<div class="ui piled segment">
  <h1 class="ui header">{{ $page->title }}</h1>
  <div id="date" style="text-align: right;">
    <h5>报名时间 : {{ substr($page->start_register_time,0,10) }} 至 {{ substr($page->end_register_time,0,10) }}
    &nbsp; &nbsp;
      <?php
        if (substr($page->end_register_time, 0, 10) >= date('Y-m-d')) {
          $willShow = true;
        } else{
          $willShow = false;
        }
      ?>

      @if ((Auth::guest() || Auth::User()->is_admin<2) && $willShow)
          @if ($is_registered) 
            <a href="{{ URL('unregistration/activity/'.$page->id) }}">取消报名</a>
          @else
            <a href="{{ URL('registration/activity/'.$page->id) }}">我要报名</a>
            &nbsp; &nbsp;
            <a href="{{ URL('registration/activity/'.$page->id.'/edit_profile') }}">添加家属报名</a>
          @endif
      @endif
    </h5>
  </div>
  <hr>
  <div id="content" style="padding: 50px;">
    <p>
      <?php echo preg_replace('/(\.\.\/){3,4}/', '../../', $page->content); ?>
    </p>
  </div>
  <div id="content" style="padding: 50px;">
    <table class="ui celled striped table">
    <?php 
      $count = 0;
      foreach ($users as $user) {
          $count += $user->number+1;
      }
      if (!Auth::guest() && Auth::User()->is_admin != null) {
        echo "<thead><tr><th colspan='4'>报名人员名单&nbsp;&nbsp;当前报名人数 : <span id='total_count' attr='".$page->id."'>".$count."</span> 人 </th></tr></thead>";
        echo "<thead><tr><th>姓名</th><th>身份证</th><th>手机</th><th>家属个数</th></tr></thead>";
        echo "<tbody>";
        foreach ($users as $user) {
          echo "<tr>";
          echo "<td>".$user->user_name."</td>";
          echo "<td>".$user->user_identi_card."</td>";
          echo "<td>".$user->user_phone."</td>";
          echo "<td>".$user->number."</td>";
          echo "</tr>";
        }
        echo "</tbody>";
      } else {
        echo "<thead><tr><th colspan='2'>报名人员名单&nbsp;&nbsp;当前报名人数 : <span id='total_count' attr='".$page->id."'>".$count."</span> 人 </th></tr></thead>";
        echo "<thead><tr><th>姓名</th><th>家属个数</th></tr></thead>";
        echo "<tbody>";
        foreach ($users as $user) {
          echo "<tr>";
          echo "<td>".$user->user_name."</td>";
          echo "<td>".$user->number."</td>";
          echo "</tr>";
        }
        echo "</tbody>";
      }
    ?>
    </table>
  </div>

  @if (!Auth::guest() && Auth::User()->is_admin != null)
    <div style="text-align:center">
      <button type="button" class="blue ui button" onClick='window.open("{{ URL('registration/activity/'.$page->id.'/export') }}")'>导出</button>
    </div>
  @endif
</div>
@endsection